package drawing

class Drawing(command: String) {

  def display = """----------------------
                  ||                    |
                  ||                    |
                  ||                    |
                  ||                    |
                  |----------------------""".stripMargin
}
