package drawing

import org.specs2.mutable.Specification

class DrawingSpec extends Specification {

  "Drawing test spec" >> {
    "Create a blank canvas" >> {
      new Drawing("C 20 4").display must_==
      """----------------------
        ||                    |
        ||                    |
        ||                    |
        ||                    |
        |----------------------""".stripMargin
    }
  }
}