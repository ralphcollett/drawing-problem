Please implement a simple text (ASCII) based drawing program (something similar to a simplified version of Paint). The basic program should allow users to:
Create a new canvas
Draw on the canvas using text based commands
Quit the program

Your solution should run 'out of the box' and interactively (i.e. it should respond to commands entered manually, see below). If you want to share comments or instructions, please include them in a README.
Commands
Example
Description
C w h	Create a new canvas of width w and height h
L x1 y1 x2 y2	Draw a new line from coordinates (x1, y1) to (x2, y2). Only horizontal or vertical lines are supported. Draw lines with the x character
R x1 y1 x2 y2	Draw a new rectangle, with upper left corner at coordinate (x1,y1) and lower right coordinate at (x2,y2). Draw lines with the x character
B x y c
Block fill the entire area starting at coordinate x,y) with a "colour" denoted by c. Google "bucket fill" if you can't work out the algorithm.
Q	Quit the program

Examples
A sample run of the program is show below. The user input is prefixed with enter command:
enter command: C 20 4
----------------------
|                    |
|                    |
|                    |
|                    |
----------------------

enter command: L 1 2 6 2
----------------------
|                    |
|xxxxxx              |
|                    |
|                    |
----------------------

enter command: L 6 3 6 4
----------------------
|                    |
|xxxxxx              |
|     x              |
|     x              |
----------------------

enter command: R 16 1 20 3
----------------------
|               xxxxx|
|xxxxxx         x   x|
|     x         xxxxx|
|     x              |
----------------------

enter command: B 10 3 o
----------------------
|oooooooooooooooxxxxx|
|xxxxxxooooooooox   x|
|     xoooooooooxxxxx|
|     xoooooooooooooo|
----------------------

enter command: Q
